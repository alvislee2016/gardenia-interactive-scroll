import React from 'react';
import { render } from "react-dom";
import Root from "./js/components/Root.jsx";

render(<Root />, document.getElementById("root"));