export function drawArc(cx, cy, radius, startAngle, endAngle) {
   
    let delta = endAngle - startAngle;       
    if (delta === 2 * Math.PI) {         
        let path =  "M " + (cx - radius) + " " + cy + " a " + radius + " " + radius + " 0 1 0 ";
        path += radius * 2 + " 0 a " + radius + " " + radius + " 0 1 0 " + -radius * 2 + " 0z"; 
        return path;
    }
    
    let largeArc = delta > Math.PI ? 1 : 0;
      
    startAngle = startAngle - Math.PI / 2;
    endAngle = endAngle - Math.PI / 2;   
    let x1 = cx + radius * Math.cos(endAngle);   
    let y1 = cy + radius * Math.sin(endAngle);   
    let x2 = cx + radius * Math.cos(startAngle); 
    let y2 = cy + radius * Math.sin(startAngle);
    let path =  "M " + x1 + " " + y1 + " A " + radius + " " + radius + " 0 " + largeArc + " 0 ";
    path += x2 + " " + y2 + " L " + cx + " " + cy + "z";
    return path
}