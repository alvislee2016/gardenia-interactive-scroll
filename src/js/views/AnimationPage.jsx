import React, { Component } from "react";
import { select } from "d3-selection";
import { TweenLite } from "gsap/TweenLite";

class AnimationPage extends Component {

    constructor(props) {
        super(props);
        this.tween = null;
        this.createD3Object = this.createD3Object.bind(this);
    }

    componentDidMount() {
        this.createD3Object();
        this.tween = TweenLite.from(this.rect, 3, { scaleX: 0 })
    }

    createD3Object() {
        const margin = 100;

        const svg = this.svg;
        const group = select(svg).append("g").attr("transform", `translate(${margin}, ${margin})`);
        this.rect = group.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", 900)
            .attr("height", 50)
            .attr("fill", "green")
            .node();
    }

    render() {
        return(
            <div>
                <h1 style={{margin:"40px auto"}}>Animation</h1>
                <p>D3 provides great support for animation with functions like duration(), delay() and ease().</p>
                <p>An easier way to do this is to use a <b>Tweening Library</b>.</p>
                <p>Tweening is the process of generating intermediate frames to give the process of animation.</p>
                <p><b>GSAP</b> is a Tweening library for JavaScript and HTML5 animation.</p>
                <p><b>TweenJS</b> is a Tweening library for JavaScript and HTML5 animation.</p>
                <h3 style={{margin:"30px auto"}}>Why GSAP</h3>
                <ul>
                    <li>It can interpolate anything including DOM elements, CSS properties and attributes</li>
                    <li>It is easy to use, well documented and has a large community</li>
                    <li>It has cool effects like DrawSVG, ScrambleText, MorphSVG and much more (membership only)</li>
                    <li>It is compatible with <b>ScrollMagic</b></li>
                </ul>
                <svg ref={node => this.svg = node} width={1100} height={250} style={{marginBottom:"20px", border:"1px solid black"}}></svg>  
                <button onClick={e => this.tween.play()}>Play</button>
                <button onClick={e => this.tween.pause()}>Pause</button>
                <button onClick={e => this.tween.reverse()}>Reverse</button>
                <button onClick={e => this.tween.restart()}>Restart</button>
                <h3 style={{margin:"30px auto"}}>Using GSAP with D3JS</h3>
                <p>GSAP and D3 both want to control the DOM.</p>
                <p>Create DOM elements with D3 and pass node reference to GSAP for tweening</p>
            </div>
        );
    }
    
};
export default AnimationPage;