import React, { Component } from "react";
import { select } from "d3-selection";


class ElementPage extends Component {

    constructor(props) {
        super(props);

        this.createD3Object = this.createD3Object.bind(this);
    }

    componentDidMount() { this.createD3Object(); }

    createD3Object() {
        const margin = 100;

        const svg = this.svg;
        const group = select(svg).append("g").attr("transform", `translate(${margin}, ${margin})`);
        group.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", 800)
            .attr("height", 50)
            .attr("fill", "green");
    }

    render() {
        return(
            <div>
                <h1 style={{margin:"40px auto"}}>Creating DOM Elements</h1>
                <p>D3 is an extremely powerful visualization tool to create interactive data visualizations.</p>
                <p>D3 allows you to manipulate the Document Object Model (DOM) based on your data.</p>
                <p>Since D3 works with web standards, it gives you complete control over your visualization features.</p>
                <svg ref={node => this.svg = node} width={1000} height={250} style={{border:"1px solid black"}}></svg>
                <h3 style={{margin:"30px auto"}}>Using D3JS with ReactJS</h3>
                <p>The challenge of integrating D3 with React is that React and D3 both want to control the DOM.</p>
                <p>Use React to build the structure of the application and render traditional HTML, </p>
                <p>Then pass DOM contianer (SVG) over to D3, so that D3 can create, destory or update elements within the container.</p>             
            </div>
        );
    }
    
};
export default ElementPage;