import React, { Component } from "react";
import { select } from "d3-selection";
import { TweenLite } from "gsap/TweenLite";
import ScrollMagic from "scrollmagic";


class ScrollPage extends Component {

    constructor(props) {
        super(props);
        this.tween = null;
        this.createD3Object = this.createD3Object.bind(this);
        this.controller = new ScrollMagic.Controller({ 
            addIndicators: true
        }); 
    }

    componentDidMount() {
        this.createD3Object();
        this.tween = TweenLite.from(this.rect, 3, { scaleX: 0 })
        new ScrollMagic.Scene({
            triggerHook: 0.9,

            duration: 500
        })
        .setTween(this.tween)
        .addTo(this.controller);
    }

    componentWillUnmount() {
        this.controller.destroy(true);
    }

    createD3Object() {
        const margin = 100;

        const svg = this.svg;
        const group = select(svg).append("g").attr("transform", `translate(${margin}, ${margin})`);
        this.rect = group.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", 800)
            .attr("height", 50)
            .attr("fill", "green")
            .node();
    }

    render() {
        return(
            <div>
                <h1 style={{margin:"40px auto"}}>Animation and Scrolling</h1>
                <p><b>skrollr</b> is a library to control scrolling. It was once popular but was deprecated since 2014.</p>
                <p><b>ScrollMagic</b> is a library to control scrolling.</p>
                <h3 style={{margin:"30px auto"}}>Why ScrollMagic</h3>
                <ul>
                    <li>It can react to user's current scroll position</li>
                    <li>It is easy to use and well documented</li>
                    <li>It comes with great debuggin tools</li>
                    <li>It is compatible with <b>GSAP</b></li>
                </ul>
                <svg ref={node => this.svg = node} width={1000} height={250} style={{border:"1px solid black"}}></svg> 
                <div style={{minHeight:"1000px"}}></div>
            </div>
        );
    }
    
};
export default ScrollPage;