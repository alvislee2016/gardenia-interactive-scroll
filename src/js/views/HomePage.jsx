import React, { Component } from "react";
import { Link } from "react-router-dom";


class HomePage extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        return(
            <div>
                <h1>Home Page</h1>
                <ol>
                    <li><Link to="d3js">Creating DOM Element</Link></li>
                    <li><Link to="gsap">Animation</Link></li>
                    <li><Link to="scrollmagic">Animation Tied to Scrolling</Link></li>
                </ol>
                <p>To create an animation, just:</p>
                <ol>
                    <li>Create React container and pass to D3.</li>
                    <li>Create D3 object and pass reference to GSAP.</li>
                    <li>Create Tween using GSAP to make animation</li>
                    <li>Create ScrollMagic Scene with GSAP animation.</li>
                </ol>
                <p>NPM has react-gsap and react-magicscroll to install. But it was only developed for 5 months and there was not much documentation.</p>
                <button><Link to="/demo">Demos</Link></button>
            </div>
        );
    }
    
};
export default HomePage;