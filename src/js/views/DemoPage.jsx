import React, { Component } from "react";
// Import GSAP
import { TweenLite } from "gsap/TweenLite";
import { CSSPlugin } from "gsap/CSSPlugin";
// Import ScrollMagic
import ScrollMagic from "scrollmagic";
// Import Reactstrap
import { Row, Col } from "reactstrap";

import ScatterChart from "../components/ScatterChart.jsx";
import BarChart from "../components/BarChart.jsx";
import DonutChart from "../components/DonutChart.jsx";
import PieChart from "../components/PieChart.jsx";
import Pins from "../components/Pins.jsx";

class DemoPage extends Component {

    constructor(props) {
        super(props);
        this.tween = null;
        this.controller = new ScrollMagic.Controller(); 
    }

    componentDidMount() {

        this.tween = TweenLite.to("#scrolldown", 1, { opacity: 0 });
        new ScrollMagic.Scene({
            offset: 100,
            duration: "45%"
        })
        .setTween(this.tween)
        .addTo(this.controller);
    }

    componentWillUnmount() {
        this.controller.destroy(true);
    }

    render() {
        return(
            <div>
                <h1 id="scrolldown">Scroll Down</h1>
                <ScatterChart controller={this.controller}/>
                <BarChart controller={this.controller}/>
                <DonutChart controller={this.controller}/>      
                <Row>
                    <Col><PieChart controller={this.controller}/></Col>
                    <Col><Pins controller={this.controller}/></Col>
                </Row>
            </div>
        );
    }
    
};
export default DemoPage;