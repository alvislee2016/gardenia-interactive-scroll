import React, { Component } from "react";
import ScrollMagic from "scrollmagic";

class Pins extends Component {

    constructor(props) {
        super(props);

        this.texts = [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at imperdiet velit. Vestibulum ut nunc dolor. Fusce consectetur lacus a.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at imperdiet velit. Vestibulum ut nunc dolor. Fusce consectetur lacus a.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at imperdiet velit. Vestibulum ut nunc dolor. Fusce consectetur lacus a.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at imperdiet velit. Vestibulum ut nunc dolor. Fusce consectetur lacus a.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at imperdiet velit. Vestibulum ut nunc dolor. Fusce consectetur lacus a.",
        ]
    }

    componentDidMount() {
        const { controller } = this.props;
        this.texts.forEach((_item, index) => {
            new ScrollMagic.Scene({
                triggerElement: `#trigger-${index}`,
                triggerHook: 0.5, 
                duration: 700
            })
            .setPin(`#element-${index}`)
            .addTo(controller);
        })
    }

    render() {
        return(
            <div>
                <div style={{minHeight:"2000px"}}></div>
            {
                this.texts.map((text, index) => {
                    return (
                        <div id={`trigger-${index}`} key={index}>
                            <p id={`element-${index}`}>{text}</p>
                        </div>
                    );
                })
            }
            </div>
        );
    }
}

export default Pins;