import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";

import configureStore from "../store";
import App from "./App.jsx";

const store = configureStore();

const Root = () => (
    <Provider store={store}>
        <Router>
            <App />
        </Router>
    </Provider>  
);

export default Root;