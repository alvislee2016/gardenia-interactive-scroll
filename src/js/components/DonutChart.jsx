import React, { Component } from "react";
// Import D3js
import { scaleOrdinal } from "d3-scale";
import { select } from "d3-selection";
import { pie, arc } from "d3-shape";
// Import GSAP
import { TweenLite } from "gsap/TweenLite";
import { TimelineLite } from "gsap/TimelineLite";
import { CSSPlugin } from "gsap/CSSPlugin";
// Import ScrollMagic
import ScrollMagic from "scrollmagic";

class DonutChart extends Component {

    constructor(props) {
        super(props);
        this.tween = new TimelineLite();
        this.createDonutChart = this.createDonutChart.bind(this);
    }

    componentDidMount() {
        // Create D3 Chart
        this.createDonutChart();
        // Create GSAP Tween
        const dasharrays = [] // Set stroke-dasharray to totalpath 
        const dashoffsets = []; // Set stroke-dashoffset 
        this.arcs.forEach(arc => {
            dasharrays.push(TweenLite.set(arc, { strokeDasharray: arc.getTotalLength() }));
            dashoffsets.push(TweenLite.fromTo(arc, 3, { strokeDashoffset: arc.getTotalLength() }, { strokeDashoffset: 0 }));
        })
        this.tween
            .add(dasharrays)
            .add(dashoffsets)
            .from(this.arcs, 1, { fill: "none" });
        // Create ScrollMagic Scene
        const { controller } = this.props;
        new ScrollMagic.Scene({
            triggerElement: "#trigger-donutchart",
            triggerHook: 0.2,
            duration: 3000
        })
        .setPin("#donutchart")
        .setTween(this.tween)
        .addTo(controller);

    }

    createDonutChart() {
        const margin = 20;
        const radius = 700 / 2 - margin;

        const svg = this.svg;
        const chart = select(svg).append("g").attr("transform", `translate(${1000 / 2}, ${700 / 2})`);

        // Create data
        let sample = pie()([2, 4, 6, 8, 10]);
        const colors = ["#3300FF", "#3333FF", "#3366FF", "#3399FF", "#33CCFF"];

        // Create Color scale
        const colorScale = scaleOrdinal(colors);

        // Create arc
        const arcs = chart.selectAll().data(sample).enter().append("g");
        this.arcs = arcs.append("path")
            .attr("d", arc().innerRadius(200).outerRadius(radius))
            .attr("fill", (s, i) => colorScale(i))
            .attr("stroke-width", 2)
            .attr("stroke", (s, i) => colorScale(i))
            .nodes();     
    }

    render() {
        return(
            <div id="trigger-donutchart">
                <div id="donutchart">
                    <svg ref={node => this.svg = node} width={1000} height={1000}></svg>
                </div>
            </div>
        );
    }
}
export default DonutChart;