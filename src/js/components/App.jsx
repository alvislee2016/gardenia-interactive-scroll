import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import { Container } from "reactstrap";

import HomePage from "../views/HomePage.jsx";
import DemoPage from "../views/DemoPage.jsx";
import ElementPage from "../views/ElementPage.jsx";
import AnimationPage from "../views/AnimationPage.jsx";
import ScrollPage from "../views/ScrollPage.jsx";

import "../../scss/main.scss";
// ScrollMagic Imports
import "imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js";
import "imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js";

class App extends Component {
    constructor() {
        super();
    }

    render() {
        return(
            <Container>
                <Switch>
                    <Route exact path="/" component={HomePage} />
                    <Route path="/demo" component={DemoPage} />
                    <Route path="/d3js" component={ElementPage} />
                    <Route path="/gsap" component={AnimationPage} />
                    <Route path="/scrollmagic" component={ScrollPage} />
                </Switch>
            </Container>
        );
    }
}

export default App;