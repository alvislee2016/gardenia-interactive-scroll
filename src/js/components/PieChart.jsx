import React, { Component } from "react";
// Import D3js
import { scaleOrdinal } from "d3-scale";
import { select } from "d3-selection";
import { pie, arc } from "d3-shape";
// Import GSAP
import { TweenLite } from "gsap/TweenLite";
import { TimelineLite } from "gsap/TimelineLite";
import { CSSPlugin } from "gsap/CSSPlugin";
import { AttrPlugin } from "gsap/AttrPlugin";
// Import ScrollMagic
import ScrollMagic from "scrollmagic";

import { drawArc } from "../helpers";

class PieChart extends Component {

    constructor(props) {
        super(props);
        
        this.createPieChart = this.createPieChart.bind(this);
        this.tween = new TimelineLite();
        this.updateArc = this.updateArc.bind(this);
    }

    componentDidMount() {
        // Create D3 Chart
        this.createPieChart();
        // Create GSAP Tween
        let endAngles = this.sample.map(s => s.endAngle);
        this.tween
            .staggerFrom(this.sample, 1, { strokeWidth: 0, cycle: { startAngle: endAngles }, onUpdate: this.updateArc, onUpdateParams:["{self}"] }, 1)
            .to(this.chart, 3, { rotation: 140, scale: 0.5, transformOrigin: "50% 50%", x: 250})
            .set(this.svg, { width: 450 })
            //.to(this.chart, 1, { opacity: 1, delay: 0.5 })
            .to(this.chart, 2, { rotation: 200, delay: 2.16 })
            .to(this.chart, 2, { rotation: 280, delay: 2.16 })
            .to(this.chart, 2, { rotation: 380, delay: 2.16 })
            .to(this.chart, 2, { rotation: 465, delay: 2.16 })
        // Create ScrollMagic Scene
        const { controller } = this.props;
        new ScrollMagic.Scene({
            triggerElement: "#trigger-piechart",
            triggerHook: 0.2,
            duration: 5400
        })
        .setPin("#piechart")
        .setTween(this.tween)
        .addTo(controller);
    }

    createPieChart() {
        const margin = 20;
        this.radius = 700 / 2 - margin;

        const svg = this.svg;
        const chart = select(svg).append("g").attr("transform", `translate(${1000 / 2}, ${700 / 2})`);
        this.chart = chart.node();

        // Create data
        this.sample = pie()([2, 4, 6, 8, 10]);
        const colors = ["#3300FF", "#3333FF", "#3366FF", "#3399FF", "#33CCFF"];

        // Create Color scale
        const colorScale = scaleOrdinal(colors);

        // Create arc
        const arcs = chart.selectAll().data(this.sample).enter().append("g");
        this.arcs = arcs.append("path")
            .style("opacity", 0)
            .attr("d", arc().innerRadius(0).outerRadius(this.radius))
            .attr("fill", (s, i) => colorScale(i))
            .attr("stroke-width", 0)
            .attr("stroke", (s, i) => colorScale(i))
            .nodes();
        
    }

    updateArc(tween) {
        let target = tween.target;
        select(this.arcs[target.index])
            .style("opacity", 1)
            .attr("d", drawArc(0, 0, this.radius, target.startAngle, target.endAngle));
    }
      

    render() {
        return(
            <div id="trigger-piechart">
                <div id="piechart">
                    <svg ref={node => this.svg = node} width={1000} height={1000} ></svg>
                </div>
            </div>
        );
    }
}
export default PieChart;