import React, { Component } from "react";
// Import D3js
import { scaleLinear, scaleBand } from "d3-scale";
import { max } from "d3-array";
import { select } from "d3-selection";
import { axisLeft, axisBottom } from "d3-axis";
// Import GSAP
import { TweenLite } from "gsap/TweenLite";
import { TimelineLite } from "gsap/TimelineLite";
// Import ScrollMagic
import ScrollMagic from "scrollmagic";

class BarChart extends Component {

    constructor(props) {
        super(props);
        this.tween = new TimelineLite();
        this.createBarChart = this.createBarChart.bind(this);
    }

    componentDidMount() { 
        // Create D3 Chart
        this.createBarChart(); 
        // Create GSAP Tween
        this.tween
            .add([
                TweenLite.from(this.yaxis, 1, { scaleY: 0, transformOrigin: "bottom" }),
                TweenLite.from(this.xaxis, 1, { scaleX: 0, transformOrigin: "left"}),
                TweenLite.from(this.yaxisLabel, 1, { opacity: 0 }),
                TweenLite.from(this.xaxisLabel, 1, { opacity: 0 })
            ])
            .staggerFrom(this.bars, 1, { scaleY: 0, transformOrigin: "bottom" } , 0.1);
        // Create ScrollMagic Scene
        const { controller } = this.props;
        new ScrollMagic.Scene({
            triggerElement: "#trigger-barchart",
            triggerHook: 0.2,
            duration: 3000
        })
        .setPin("#barchart")
        .setTween(this.tween)
        .addTo(controller);
    }


    createBarChart() {
        const margin = 60;
        const width = 1000 - 2 * margin;
        const height = 600 - 2 * margin;

        const svg = this.svg;
        // Translate chart by (left margin, top margin)
        const chart = select(svg).append("g").attr("transform", `translate(${margin}, ${margin})`);

        // Create data
        const sample = [];
        for(let i = 0; i < 10; i++) {
            let label = `label-${i}`;
            let value = Math.floor(Math.random() * 100 + 1);
            sample.push({ label: label, value: value });
        }

        // Create X-axis
        const xScale = scaleBand().range([0, width]).domain(sample.map(s => s.label)).padding(0.2);
        this.xaxis = chart.append("g").attr("transform", `translate(0, ${height})`).call(axisBottom(xScale)).node();
        this.xaxisLabel = select(svg).append("text")
            .attr("x", width / 2 + margin)
            .attr("y", height + margin * 1.7)
            .attr("text-anchor", "middle")
            .text("X Axis")
            .node();
        
        // Create Y-axis
        const maxY = max(sample, s => s.value);
        const yScale = scaleLinear().range([height, 0]).domain([0, maxY]);
        this.yaxis = chart.append("g").call(axisLeft(yScale)).node();
        this.yaxisLabel = select(svg).append("text")
            .attr("x", -(height / 2) + margin)
            .attr("y", margin / 2.4)
            .attr("transform", "rotate(-90)")
            .attr("text-anchor", "middle")
            .text("Y Axis")
            .node();

        // Create Bars
        const bars = chart.selectAll().data(sample).enter().append("g");
        this.bars = bars.append("rect")
            .attr("x", s => xScale(s.label))
            .attr("y", s => yScale(s.value))
            .attr("height", s => height - yScale(s.value))
            .attr("width", xScale.bandwidth())
            .attr("fill", "blue")
            .nodes();
    }

    render() {
        return(
            <div id="trigger-barchart">
                <div id="barchart">
                    <svg ref={node => this.svg = node} width={1000} height={600}></svg>
                </div>
            </div>
        );
    }
}

export default BarChart;